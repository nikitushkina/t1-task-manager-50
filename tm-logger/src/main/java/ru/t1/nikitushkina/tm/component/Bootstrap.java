package ru.t1.nikitushkina.tm.component;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import ru.t1.nikitushkina.tm.Application;
import ru.t1.nikitushkina.tm.listener.EntityListener;
import ru.t1.nikitushkina.tm.service.LoggerService;

import javax.jms.*;

public final class Bootstrap {

    @SneakyThrows
    public static void startLogger() {
        final LoggerService loggerService = new LoggerService();
        final EntityListener entityListener = new EntityListener(loggerService);
        final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Application.URL);
        final Connection connection = connectionFactory.createConnection();
        connection.start();
        final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        final Queue destination = session.createQueue(Application.QUEUE);
        final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }

}
