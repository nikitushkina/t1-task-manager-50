package ru.t1.nikitushkina.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String APPLICATION_FILE_NAME_KEY = "config";
    @NotNull
    public static final String APPLICATION_FILE_NAME_DEFAULT = "application.properties";
    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";
    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";
    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";
    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "42";
    @NotNull
    public static final String PASSWORD_ITERATION_KEY = "password.iteration";
    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "1488";
    @NotNull
    public static final String PASSWORD_SECRET_KEY = "password.secret";
    @NotNull
    public static final String EMPTY_VALUE = "-";
    @NotNull
    public static final String GIT_BRANCH = "gitBranch";
    @NotNull
    public static final String GIT_COMMIT_ID = "gitCommitId";
    @NotNull
    public static final String GIT_COMMIT_TIME = "gitCommitTime";
    @NotNull
    public static final String GIT_COMMIT_MSG_FULL = "gitCommitMsgFull";
    @NotNull
    public static final String GIT_COMMITTER_NAME = "gitCommitterName";
    @NotNull
    public static final String GIT_COMMITTER_EMAIL = "gitCommitterEmail";
    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";
    @NotNull
    private static final String SERVER_PORT_DEFAULT = "8080";
    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";
    @NotNull
    private static final String SERVER_HOST_DEFAULT = "127.0.0.1";
    @NotNull
    private static final String ADMIN_LOGIN_KEY = "admin.login";
    @NotNull
    private static final String ADMIN_PASSWORD_KEY = "admin.password";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        final boolean existsConfig = isExistExternalConfig();
        if (existsConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @NotNull
    @Override
    public String getAdminLogin() {
        return getStringValue(ADMIN_LOGIN_KEY, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getAdminPassword() {
        return getStringValue(ADMIN_PASSWORD_KEY, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getCommitId() {
        return read(GIT_COMMIT_ID);
    }

    @NotNull
    @Override
    public String getCommitMsgFull() {
        return read(GIT_COMMIT_MSG_FULL);
    }

    @NotNull
    @Override
    public String getCommitTime() {
        return read(GIT_COMMIT_TIME);
    }

    @NotNull
    @Override
    public String getCommitterEmail() {
        return read(GIT_COMMITTER_EMAIL);
    }

    @NotNull
    @Override
    public String getCommitterName() {
        return read(GIT_COMMITTER_NAME);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    @Override
    public String getGitBranch() {
        return read(GIT_BRANCH);
    }

    @NotNull
    @Override
    public String getHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    private Integer getIntegerValue(
            @NotNull final String key,
            @NotNull final String defaultKey
    ) {
        return Integer.parseInt(getStringValue(key, defaultKey));
    }

    @NotNull
    @Override
    public String getPort() {
        return getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @NotNull
    private String getStringValue(
            @NotNull final String key,
            @NotNull final String defaultValue
    ) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    private boolean isExistExternalConfig() {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }

    @SneakyThrows
    private void loadExternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        @Cleanup @NotNull final InputStream inputStream = new FileInputStream(file);
        properties.load(inputStream);
    }

    @SneakyThrows
    private void loadInternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = APPLICATION_FILE_NAME_DEFAULT;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

}
