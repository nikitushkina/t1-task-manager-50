package ru.t1.nikitushkina.tm.migration;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.junit.Test;
import ru.t1.nikitushkina.tm.service.ConnectionService;
import ru.t1.nikitushkina.tm.service.PropertyService;

public class SchemeTest extends AbstractSchemeTest {

    @Test
    public void Test() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        PropertyService propertyService = new PropertyService();
        ConnectionService connectionService = new ConnectionService(propertyService);
    }

}
