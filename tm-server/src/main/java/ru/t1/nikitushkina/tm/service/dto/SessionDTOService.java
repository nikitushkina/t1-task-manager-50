package ru.t1.nikitushkina.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.nikitushkina.tm.api.service.IConnectionService;
import ru.t1.nikitushkina.tm.api.service.dto.ISessionDTOService;
import ru.t1.nikitushkina.tm.dto.model.SessionDTO;
import ru.t1.nikitushkina.tm.repository.dto.SessionDTORepository;

import javax.persistence.EntityManager;

public final class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO, ISessionDTORepository>
        implements ISessionDTOService {

    public SessionDTOService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected ISessionDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionDTORepository(entityManager);
    }

}
