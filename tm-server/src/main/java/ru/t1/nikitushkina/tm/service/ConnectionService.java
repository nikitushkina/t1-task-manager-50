package ru.t1.nikitushkina.tm.service;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.SneakyThrows;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.api.service.IConnectionService;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;
import ru.t1.nikitushkina.tm.dto.model.ProjectDTO;
import ru.t1.nikitushkina.tm.dto.model.SessionDTO;
import ru.t1.nikitushkina.tm.dto.model.TaskDTO;
import ru.t1.nikitushkina.tm.dto.model.UserDTO;
import ru.t1.nikitushkina.tm.model.Project;
import ru.t1.nikitushkina.tm.model.Session;
import ru.t1.nikitushkina.tm.model.Task;
import ru.t1.nikitushkina.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public final class ConnectionService implements IConnectionService {

    private static final ClassLoaderResourceAccessor ACCESSOR = new ClassLoaderResourceAccessor();

    private static Database DATABASE;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(
            @NotNull final IPropertyService propertyService
    ) {
        this.propertyService = propertyService;
        this.entityManagerFactory = factory();
    }

    @Override
    @SneakyThrows
    public void close() {
        entityManagerFactory.close();
    }

    @NotNull
    @SneakyThrows
    private EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getDBDriver());
        settings.put(Environment.URL, propertyService.getDBUrl());
        settings.put(Environment.USER, propertyService.getDBUser());
        settings.put(Environment.PASS, propertyService.getDBPassword());
        settings.put(Environment.DIALECT, propertyService.getDBDialect());
        settings.put(Environment.HBM2DDL_AUTO, propertyService.getDBHbm2ddlAuto());
        settings.put(Environment.SHOW_SQL, propertyService.getDBShowSql());
        settings.put(Environment.FORMAT_SQL, propertyService.getFormatSQL());
        settings.put(Environment.DEFAULT_SCHEMA, propertyService.getDBSchema());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDBSecondLvlCache());
        settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getDBFactoryClass());
        settings.put(Environment.USE_QUERY_CACHE, propertyService.getDBUseQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getDBUseMinPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getDBRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDBConfigFilePath());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    private Connection getConnection(Properties properties) throws SQLException {
        return DriverManager.getConnection(
                properties.getProperty("url"),
                properties.getProperty("username"),
                properties.getProperty("password")
        );
    }

    @NotNull
    @Override
    @SneakyThrows
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    @Override
    @SneakyThrows
    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Liquibase getLiquibase() {
        return liquibase(propertyService.getLiquibaseConfig());
    }

    private void initLiquibase() throws IOException, SQLException, DatabaseException {
        final Properties properties = new Properties();
        final InputStream inputStream = ClassLoader.getSystemResourceAsStream("liquibase.properties");
        properties.load(inputStream);
        final Connection connection = getConnection(properties);
        final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        DATABASE = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
    }

    @SneakyThrows
    public Liquibase liquibase(final String filename) {
        initLiquibase();
        return new Liquibase(filename, ACCESSOR, DATABASE);
    }

}
