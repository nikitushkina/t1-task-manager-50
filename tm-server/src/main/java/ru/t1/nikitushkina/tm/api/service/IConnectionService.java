package ru.t1.nikitushkina.tm.api.service;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    void close();

    @NotNull
    EntityManager getEntityManager();

    @NotNull
    EntityManagerFactory getEntityManagerFactory();

    @NotNull
    Liquibase getLiquibase();

}
