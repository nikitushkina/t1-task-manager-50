package ru.t1.nikitushkina.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;

public interface IProjectTaskDTOService {

    void bindTaskToProject(@Nullable String userId,
                           @Nullable String projectId,
                           @Nullable String taskId);

    void removeProjectById(@Nullable String userId, @Nullable String projectId);

    void unbindTaskFromProject(@Nullable String userId,
                               @Nullable String projectId,
                               @Nullable String taskId);

}
