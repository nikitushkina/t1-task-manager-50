package ru.t1.nikitushkina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.endpoint.IAuthEndpoint;
import ru.t1.nikitushkina.tm.api.service.IAuthService;
import ru.t1.nikitushkina.tm.api.service.IServiceLocator;
import ru.t1.nikitushkina.tm.api.service.dto.IUserDTOService;
import ru.t1.nikitushkina.tm.dto.model.SessionDTO;
import ru.t1.nikitushkina.tm.dto.model.UserDTO;
import ru.t1.nikitushkina.tm.dto.request.UserLoginRequest;
import ru.t1.nikitushkina.tm.dto.request.UserLogoutRequest;
import ru.t1.nikitushkina.tm.dto.request.UserProfileRequest;
import ru.t1.nikitushkina.tm.dto.response.UserLoginResponse;
import ru.t1.nikitushkina.tm.dto.response.UserLogoutResponse;
import ru.t1.nikitushkina.tm.dto.response.UserProfileResponse;
import ru.t1.nikitushkina.tm.exception.EndpointException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.nikitushkina.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IAuthService getAuthService() {
        return this.getServiceLocator().getAuthService();
    }

    @NotNull
    private IUserDTOService getUserService() {
        return this.getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginRequest request
    ) {
        try {
            @NotNull final String token = getAuthService().login(request.getLogin(), request.getPassword());
            return new UserLoginResponse(token);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLogoutRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        try {
            getAuthService().logout(session);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserProfileRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable UserDTO user;
        try {
            user = getUserService().findOneById(userId);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new UserProfileResponse(user);
    }

}
