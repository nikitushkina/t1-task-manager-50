package ru.t1.nikitushkina.tm.service;

import liquibase.Liquibase;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.service.IAdminService;
import ru.t1.nikitushkina.tm.api.service.IConnectionService;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;
import ru.t1.nikitushkina.tm.exception.user.AccessDeniedException;

public class AdminService implements IAdminService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final IPropertyService propertyService;

    public AdminService(
            @NotNull IConnectionService connectionService,
            @NotNull IPropertyService propertyService) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @Override
    @SneakyThrows
    public void dropScheme(@Nullable String initToken) {
        @NotNull final String token = propertyService.getTokenInit();
        if (initToken == null || !initToken.equals(token)) throw new AccessDeniedException();
        @NotNull final Liquibase liquibase = connectionService.getLiquibase();
        liquibase.dropAll();
        liquibase.close();
    }

    @Override
    @SneakyThrows
    public void initScheme(@Nullable String initToken) {
        @NotNull final String token = propertyService.getTokenInit();
        if (initToken == null || !initToken.equals(token)) throw new AccessDeniedException();
        @NotNull final Liquibase liquibase = connectionService.getLiquibase();
        liquibase.update("scheme");
        liquibase.close();
    }

}
