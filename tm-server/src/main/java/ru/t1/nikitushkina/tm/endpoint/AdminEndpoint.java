package ru.t1.nikitushkina.tm.endpoint;

import liquibase.Liquibase;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.endpoint.IAdminEndpoint;
import ru.t1.nikitushkina.tm.api.service.IAdminService;
import ru.t1.nikitushkina.tm.api.service.IConnectionService;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;
import ru.t1.nikitushkina.tm.api.service.IServiceLocator;
import ru.t1.nikitushkina.tm.dto.request.SchemeDropRequest;
import ru.t1.nikitushkina.tm.dto.request.SchemeInitRequest;
import ru.t1.nikitushkina.tm.dto.response.SchemeDropResponse;
import ru.t1.nikitushkina.tm.dto.response.SchemeInitResponse;
import ru.t1.nikitushkina.tm.exception.EndpointException;
import ru.t1.nikitushkina.tm.exception.user.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.nikitushkina.tm.api.endpoint.IAdminEndpoint")
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    public AdminEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    private IConnectionService getConnectionService() {
        return this.getServiceLocator().getConnectionService();
    }

    private IPropertyService getPropertyService() {
        return this.getServiceLocator().getPropertyService();
    }

    @NotNull
    private IAdminService getAdminService() {
        return getServiceLocator().getAdminService();
    }

    @WebMethod
    @Override
    @SneakyThrows
    public SchemeInitResponse initScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @Nullable final SchemeInitRequest request
    ) {
        try {
            getAdminService().initScheme(request.getInitToken());
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new SchemeInitResponse();
    }

    @WebMethod
    @Override
    @SneakyThrows
    public SchemeDropResponse dropScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @Nullable final SchemeDropRequest request
            ) {
        try {
            getAdminService().dropScheme(request.getInitToken());
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new SchemeDropResponse();
    }

}
