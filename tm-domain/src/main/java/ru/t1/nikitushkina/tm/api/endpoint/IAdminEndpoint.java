package ru.t1.nikitushkina.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.dto.request.SchemeDropRequest;
import ru.t1.nikitushkina.tm.dto.request.SchemeInitRequest;
import ru.t1.nikitushkina.tm.dto.response.SchemeDropResponse;
import ru.t1.nikitushkina.tm.dto.response.SchemeInitResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAdminEndpoint extends IEndpoint {

    @NotNull
    String NAME = "AdminEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IAuthEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IAuthEndpoint.class);
    }

    @NotNull
    @WebMethod
    SchemeInitResponse initScheme(@WebParam(name = REQUEST, partName = REQUEST)
                                  @NotNull SchemeInitRequest request);

    @NotNull
    @WebMethod
    SchemeDropResponse dropScheme(@WebParam(name = REQUEST, partName = REQUEST)
                                  @NotNull SchemeDropRequest request);

}
